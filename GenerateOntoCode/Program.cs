﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.CodeDom.Compiler;
using OntologyClasses.BaseClasses;
using System.Reflection;

namespace GenerateOntoCode
{
    class Program
    {
        private static Globals globals;
        static void Main(string[] args)
        {
            globals = new Globals();
            GenerateChannels();
            GenerateCommands();
        }

        private static clsOntologyItem GenerateChannels()
        {
            var classId = "2c2c4448e72e40b0a4863f2c0998f94c";
            var dbReader = new OntologyModDBConnector(globals);

            var parameters = new CompilerParameters();

            Version version = Assembly.GetEntryAssembly().GetName().Version;

            parameters.GenerateExecutable = false;
            parameters.OutputAssembly = "OModules.Channels.dll";
            

            var result = dbReader.GetDataObjects(new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = classId
                }
            });

            if (result.GUID == globals.LogStates.LogState_Success.GUID)
            {
                var classesCode = Templates.Templates.ChannelsTemplate;
                var sbChannels = new StringBuilder();
                sbChannels.Append(Templates.Templates.ChannelsTemplate);
                sbChannels.Replace("@VERSION_NUMBER@", version.ToString());
                var sbChannelProperties = new StringBuilder();
                dbReader.Objects1.ForEach(channel =>
                {
                    sbChannelProperties.AppendLine(Templates.Templates.ChannelPropertyTemplate.Replace("@CHANNEL@", channel.Name).Replace("@CHANNEL_ID@", channel.GUID));
                    
                });

                sbChannels.Replace("@CHANNEL_LIST@", sbChannelProperties.ToString());

                CompilerResults r = CodeDomProvider.CreateProvider("CSharp").CompileAssemblyFromSource(parameters, sbChannels.ToString());
            }

            

            return result;
        }

        private static clsOntologyItem GenerateCommands()
        {
            var classId = "ce7fc146b6e7430f84fc5fcbaa11dabb";
            var dbReader = new OntologyModDBConnector(globals);

            Version version = Assembly.GetEntryAssembly().GetName().Version;
            var parameters = new CompilerParameters();

            parameters.GenerateExecutable = false;
            parameters.OutputAssembly = "OModules.Commands.dll";


            var result = dbReader.GetDataObjects(new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = classId
                }
            });

            if (result.GUID == globals.LogStates.LogState_Success.GUID)
            {
                var classesCode = Templates.Templates.CommandTemplate;
                var sbCommands = new StringBuilder();
                sbCommands.Append(Templates.Templates.CommandTemplate);
                sbCommands.Replace("@VERSION_NUMBER@", version.ToString());
                var sbCommandProperties = new StringBuilder();
                dbReader.Objects1.ForEach(Command =>
                {
                    sbCommandProperties.AppendLine(Templates.Templates.CommandPropertyTemplate.Replace("@COMMAND@", Command.Name).Replace("@COMMAND_ID@", Command.GUID));

                });

                sbCommands.Replace("@COMMAND_LIST@", sbCommandProperties.ToString());

                CompilerResults r = CodeDomProvider.CreateProvider("CSharp").CompileAssemblyFromSource(parameters, sbCommands.ToString());
            }



            return result;
        }
    }

    
}
